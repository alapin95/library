## Design descisions

1. Library class consits of two hash maps. The first one maps an item id to the item and the second one is used to lookup item's id by name.
2. There is a Borrowable interface that specifies methods that an item that can be borrowed should implement.
3. BorrowableItem was initially an abstract class that would enforce each subclass to implement the borrow method. However,
due to time constraints, all the items that can be borrowed use the borrow method from the BorrowableItem class.
4. The only distinction between the borrowed item and a not borrowed item is status. No additional logic was introduced.
5. BorrowableItems have a fields for date when the item was borrowed and when it is due to be returned.
6. An example usage was implemented in the Main.java class (no JUnit tests were added).