package com.citinam.library;

import java.time.LocalDate;

public class Book extends BorrowableItem {
	
	private String author;
	private String description;
	private String ISBN;

	public Book(String name, String author, String description, String ISBN) {
		super(name);
		this.author = author;
		this.description = description;
		this.ISBN = ISBN;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

}
