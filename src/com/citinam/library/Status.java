package com.citinam.library;

public enum Status {
	AVAILABLE,
	BORROWED
}
