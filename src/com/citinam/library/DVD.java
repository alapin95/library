package com.citinam.library;

public class DVD extends BorrowableItem {
	private String director;
	
	public DVD(String name, String director) {
		super(name);
		this.director = director;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}
	
}
