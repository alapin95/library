package com.citinam.library;

import java.time.LocalDate;

public class BorrowableItem extends Item implements Borrowable {
	
	private LocalDate dueDate;
	private Status status;
	
	public BorrowableItem(String name) {
		super(name);
		this.status = Status.AVAILABLE;
		this.setDueDate(null);
	}

	public LocalDate getDueDate() {
		return this.dueDate;
	}
	
	@Override
	public void setDueDate(Integer days) {
		if (days == null) {
			this.dueDate = null;
		} else {
			this.dueDate = LocalDate.now().plusDays(days);
		}
	}
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void borrow() {
		
		if (this.getStatus().equals(Status.BORROWED)) {
			System.out.println("This item is already borrowed.");
		}
		
		this.setDueDate(daysToBorrow);
		this.setStatus(Status.BORROWED);
		
		System.out.println(String.format("Borrowed item: %s.", this.getName()));
	}

}
