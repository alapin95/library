package com.citinam.library;

import java.util.HashMap;
import java.util.Map;

public class Library {
	private Map<Integer, Item> library;
	private Map<String, Integer> nameToId;
	
	public Library() {
		this.library = new HashMap<Integer, Item>();
		this.nameToId = new HashMap<String, Integer>();
	}
	
	public Integer lookupItemId(String name) {
		if (nameToId.containsKey(name)) {
			return nameToId.get(name);
		}
		return null;
	}
	
	public void addItem(Item newItem) {
		Integer itemId = newItem.getName().hashCode() & 0xfffffff;
		nameToId.put(newItem.getName(), itemId);
		library.put(itemId, newItem);
		
		System.out.println(String.format("Added item %s to the library.", newItem.getName()));
	}
	
	public Item removeItem(Integer itemId) {
		
		if (!library.containsKey(itemId)) {
			System.out.println(String.format("Item #%s not found in the library.", itemId));
			return null;
		}
		
		Item removedItem = library.get(itemId);
		
		nameToId.remove(itemId);
		library.remove(itemId);
		
		System.out.println(String.format("Removed item %s from the library.", removedItem.getName()));
		
		return removedItem;
	}
}
