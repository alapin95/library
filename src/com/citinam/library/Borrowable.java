package com.citinam.library;

import java.time.LocalDate;

public interface Borrowable {
	final public static Integer daysToBorrow = 30;
	public void setStatus(Status status);
	public void setDueDate(Integer days);
	public void borrow();
}
