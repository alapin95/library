package com.citinam.library;

public class Main {

	public static void main(String[] args) {
		System.out.println("Welcome to the virtual library!\n");
		
		Library library = new Library();
		
		Book book = new Book("Romeo and Juliet", "William Shakespeare", "Romeo and Juliet is a tragedy written by "
				+ "William Shakespeare early in his career about two young star-crossed lovers whose deaths ultimately "
				+ "reconcile their feuding families.", "978-1-60309-371-2");
		
		DVD movie = new DVD("The Dark Knight", "Christopher Nolan");
		
		Periodical periodical = new Periodical("Time", "Edition N123");
		
		// Add items to the library
		library.addItem(book);
		library.addItem(movie);
		library.addItem(periodical);
		
		// Attempt to borrow items
		movie.borrow();
		book.borrow();
		
		// Borrow the same book again
		book.borrow();
		
		// Remove item from the library
		library.removeItem(library.lookupItemId(movie.getName()));
		
	}

}
