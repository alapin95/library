package com.citinam.library;

public class CD extends BorrowableItem {
	private String album;
	private String composer;
	private Integer year;
	
	public CD(String name, String album, String composer, Integer year) {
		super(name);
		this.album = album;
		this.composer = composer;
		this.year = year;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
}
