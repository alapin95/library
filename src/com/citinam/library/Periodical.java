package com.citinam.library;

public class Periodical extends Item {
	private String edition;
	
	public Periodical(String name, String edition) {
		super(name);
		this.edition = edition;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}
	
}
